<?php

/**
 * @file
 * uw_nav_global_header.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_nav_global_header_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'global_header';
  $context->description = 'Menu placed in the global header.';
  $context->tag = 'Navigation';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_nav_global_header-header' => array(
          'module' => 'uw_nav_global_header',
          'delta' => 'header',
          'region' => 'global_header',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Menu placed in the global header.');
  t('Navigation');
  $export['global_header'] = $context;

  return $export;
}
